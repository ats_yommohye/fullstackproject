package it.ats.FullStackProject.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.fasterxml.jackson.core.JsonProcessingException;
import it.ats.FullStackProject.pentaho.PentahoTransformation;



@RestController
public class ProjectController 
{
	@RequestMapping("/")
	public ModelAndView index ()
	{
		//using a ModelAndView object to be able to return an html page. 
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("Index");
	    return modelAndView;
	}
	
	//when calling the URL below, the transformation will be executed 
	@RequestMapping(value = "/home/RunTrans/{transName}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> runTransfromation(@PathVariable ("transName") String transName) throws JsonProcessingException 
	{	
		try 
		{
			return new ResponseEntity<Object>(PentahoTransformation.runTransformation(transName), HttpStatus.OK);
		}
		catch (Exception e) 
		{
			return new ResponseEntity<Object>(e.getCause(), HttpStatus.BAD_REQUEST);
		}
		
	}
	
}
	
