package it.ats.FullStackProject.pentaho;


import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

import it.ats.FullStackProject.converter.XML2JSON;



public class PentahoTransformation 

{  
	//public static final String TRANS_PATH="/Users/yomna/Desktop/";

	public static String runTransformation (String transName)

	{
		Trans trans= null;
		try 
		{

			KettleEnvironment.init();
			EnvUtil.environmentInit();
			//TransMeta metaData = new TransMeta(TRANS_PATH+transName+".ktr");
			TransMeta metaData = new TransMeta(transName+".ktr");
			trans = new Trans( metaData );
			trans.execute(null);
            trans.waitUntilFinished();

           if ( trans.getErrors() > 0 ) {
				System.out.print( "Error Executing transformation" );
			}

		} 
		catch (KettleException k) 
		{
			System.out.println("The transformation can't be executed.Make sure that it's in the correct path");
			k.printStackTrace();
		}


		return XML2JSON.convert(trans.getResult().getXML());
	}
}

