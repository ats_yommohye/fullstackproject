# FullStackProject


This repository contains a full stack java project that aims to run Pentaho Kettle transformations.

**In this project there are used:**
    
*  in the back-end side :
          
        *  Maven build tool
        *  Spring boot 2.2.6.RELEASE framework
        *  REST
        
*   in the front-end side :

        *  HTML
        *  CSS
        *  JS
        *  JQUERY
        *  CSS BOOTSTRAP 4

The application homepage is composed by three buttons. Each one runs a different Kettle transformation.

By clicking on one of these an ajax call will request to the back-end to run the transformation and give a json result.

The controller will get the request,execute the transformation and wait until it's finished to give a final result.Lastly, the response will be posted in the home page textarea.

In the pom.xml file there are  dependencies necessary to execute the Kettle transformation and the corrrect Pentaho repository. They are listed here below:
*  pentaho-parent-pom  version: 5.2.0.2-84
*  kettle-engine version: 5.2.0.2-84
*  kettle-core version: 5.2.0.2-84
*  Pentaho repository : http://nexus.pentaho.org/content/groups/omni


**Command to run the application from command line:**

  *mvn spring-boot:run*